import scrapy
import csv
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher

class ProductSpider(scrapy.Spider):

    seller = "Tunisianet"
    name = "products_tunisianet"
    filename = "products_tunisianet.csv"
    fields = ["product_seller", "product_category", "product_name", "product_ref", "product_brand", "product_price", "product_link"]
    product_crawled_counter = 0
    parsed_products = list()

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        self.write_to_csv()

    def start_requests(self):
        urls = [
            'http://www.tunisianet.com.tn/368-vente-videoprojecteur-tunisie',
            'http://www.tunisianet.com.tn/316-imprimante-en-tunisie',
            'http://www.tunisianet.com.tn/317-consommable-tunisie',
            'http://www.tunisianet.com.tn/337-son',
            'http://www.tunisianet.com.tn/498-hub-usb-tunisie',
            'http://www.tunisianet.com.tn/332-clavier-souris',
            'http://www.tunisianet.com.tn/313-disque-dur-externe-tunisie',
            'http://www.tunisianet.com.tn/315-carte-memoire-tunisie',
            'http://www.tunisianet.com.tn/314-cle-usb-tunisie',
            'http://www.tunisianet.com.tn/469-accessoires-pour-stockage',
            'http://www.tunisianet.com.tn/349-cable-connectique-informatique',
            'http://www.tunisianet.com.tn/589-cahier-bloc-feuille-note',
            'http://www.tunisianet.com.tn/501-fourniture-classement-archivage-tunisie'      
        ]
        for url in urls:
            print("Crawling url under >> " + url)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response): # Crawling category pages
        for href in response.css("h2.product-title a::attr(href)").extract():
            print("Crawling product under >> " + href)
            yield scrapy.Request(url=href, callback=self.parse_product)

    def parse_product(self, response): # Crawling product
        product = dict() 

        product[self.fields[0]] = self.seller # prod_seller
        product_category = response.xpath('//*[@id="wrapper"]/div/nav/ol/li[3]/a/span/text()') # prod_category
        if product_category:
            product[self.fields[1]] = product_category.extract_first().replace(',',' ').encode('utf-8')
        else:
            product[self.fields[1]] = "N/A"

        product[self.fields[2]] = response.css('h1::text').extract_first().replace(',',' ').encode('utf-8') # prod_name
        product[self.fields[3]] = response.css('.product-reference span::text').extract_first().replace('[','').replace(']','') # prod_ref
        product_brand = response.xpath('//script[@type="text/javascript"]').re('(?<=brand\":\")(.*?)(?=\",\"var)')[0] # prod_brand
        
        if "noname" not in product_brand:
            product[self.fields[4]] = product_brand
        else:
            product[self.fields[4]] = "N/A"
        
        product[self.fields[5]] = response.xpath('//*[@id="add-to-cart-or-refresh"]/div[2]/div[1]/div/span/text()').extract_first().encode('utf-8').replace('TND','').replace('TTC','').replace(',','.').replace('.000','').replace(' ','').strip() # prod_price
        product[self.fields[6]] = response.request.url # prod_link

    
        self.product_crawled_counter += 1
        print('++++ ' + str(self.product_crawled_counter) + ' crawled products ++++')
        self.parsed_products.append(product)

    def write_to_csv(self):
        self.write_csv_headers()
        sorted_products = sorted(self.parsed_products, key=lambda k: k[self.fields[1]])
        for product in sorted_products:
            self.write_csv_entry(product)

    def write_csv_headers(self):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(field) 
                                    for field in self.fields)))
        return
        
    def write_csv_entry(self, entry):
        with open(self.filename,'a+') as f:
            for k, v in entry.items():
                print(k, v)
            f.write("{}\n".format('\t'.join(str(entry[field]) 
                                for field in self.fields)))