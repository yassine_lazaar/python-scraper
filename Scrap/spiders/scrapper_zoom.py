import scrapy
import csv
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher

class ProductSpider(scrapy.Spider):

    seller = "Zoom"
    name = "products_zoom"
    base = "http://www.zoom.com.tn"
    filename = "products_zoom.csv"
    fields = ["product_seller", "product_category", "product_name", "product_ref", "product_brand", "product_price", "product_link"]
    product_crawled_counter = 0
    parsed_products = list()

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        self.write_to_csv()

    def start_requests(self):
        urls = [
            'http://www.zoom.com.tn/770-photocopieurs-monochrome',
            'http://www.zoom.com.tn/771-photocopieurs-couleur',
            'http://www.zoom.com.tn/764-multifonction',
            'http://www.zoom.com.tn/763-imprimante',
            'http://www.zoom.com.tn/765-consommables',
            'http://www.zoom.com.tn/807-video-projecteur',
            'http://www.zoom.com.tn/987-haut-parleur-bluetooth',
            'http://www.zoom.com.tn/986-casque-ecouteurs',
            'http://www.zoom.com.tn/846-classement-archivage',
            'http://www.zoom.com.tn/705-stockage#/categories-disque_dur_externe',
            'http://www.zoom.com.tn/705-stockage#/categories-cle_usb',
            'http://www.zoom.com.tn/705-stockage#/categories-carte_memoire'
        ]
        for url in urls:
            print("Crawling url under >> " + url)
            yield scrapy.Request(url=url, callback=self.parse_category)

    def parse_category(self, response): # Crawling listed products
        for href in response.css('a.product-name::attr(href)').extract():
            print("Crawling product under >> " + href)
            yield scrapy.Request(url=href, callback=self.parse_product)
        next = response.xpath('//*[@id="pagination_next_bottom"]/a/@href').extract_first()
        if next: # Crawling next category pages
            href = self.base + next
            print("Crawling category page under >> " + href)
            yield scrapy.Request(url=href, callback=self.parse_category)

    def parse_product(self, response): # Crawling product
        product = dict()
        product[self.fields[0]] = self.seller # prod_seller
        product_category = response.xpath('//*[@id="breadcrumb"]/div/div/span[2]/a[2]/text()') # prod_category
        if product_category:
            product[self.fields[1]] = product_category.extract_first().encode('utf-8')
        else:
            product_category = response.xpath('//*[@id="breadcrumb"]/div/div/span[2]/a/text()')
            if product_category:
                product[self.fields[1]] = product_category.extract_first().encode('utf-8')
            else:
                product[self.fields[1]] = "N/A"

        product[self.fields[2]] = response.css('h1::text').extract_first().replace(',',' ').encode('utf-8') # prod_name
        product[self.fields[3]] = response.xpath('//*[@id="product_reference"]/span/text()').extract_first() # prod_ref
        product_brand = response.css('.marque_name img::attr(alt)').extract_first() # prod_brand
        if product_brand:
            product[self.fields[4]] = product_brand.encode('utf-8')
        else:
            product[self.fields[4]] = "N/A"

        product_price = response.css('#our_price_display::text') # prod_price
        if product_price:
            product[self.fields[5]] = product_price.extract_first().replace('DT','').replace(',','.').replace('.000','').replace(' ','').strip()
        else:
            product[self.fields[5]] = "N/A"
        
        product[self.fields[6]] = response.request.url # prod_link
        
        self.product_crawled_counter += 1
        print('++++ ' + str(self.product_crawled_counter) + ' crawled products ++++')
        self.parsed_products.append(product)

    def write_to_csv(self):
        self.write_csv_headers()
        sorted_products = sorted(self.parsed_products, key=lambda k: k[self.fields[1]])
        for product in sorted_products:
            self.write_csv_entry(product)

    def write_csv_headers(self):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(field) 
                                    for field in self.fields)))
        return
        
    def write_csv_entry(self, entry):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(entry[field]) 
                                for field in self.fields)))