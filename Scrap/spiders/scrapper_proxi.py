import scrapy
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import csv
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import pickle

class ProductSpider(scrapy.Spider):

    seller = "Proxi"
    name = "products_proxi"
    filename = "products_proxi.csv"
    urls_filename = "proxi_urls"
    fields = ["product_seller", "product_category", "product_name", "product_ref", "product_brand", "product_price", "product_link"]
    product_crawled_counter = 0
    parsed_products = list()

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        self.write_to_csv()

    def start_requests(self):
        with open(self.urls_filename, 'rb') as f:
            urls = pickle.load(f)
            for url in urls:
                print("Crawling url under >> " + url)
                yield scrapy.Request(url=url, callback=self.parse_product)       
    
    def parse_product(self, response): # Crawling product
        product = dict()
        product[self.fields[0]] = self.seller # prod_seller
        product_category = response.xpath('//*[@id="center_column"]/div[1]/div[1]/a[2]/text()') # prod_category
        if product_category:
            product[self.fields[1]] = product_category.extract_first().encode('utf-8')
        else:
            product[self.fields[1]] = "N/A"

        product[self.fields[2]] = response.css('span.productTitle::text').extract_first().replace(',',' ').encode('utf-8') # prod_name
        product[self.fields[3]] = response.css('#product_reference span::text').extract_first() # prod_ref
        product_brand = response.css('p.product-manufacturer img::attr("alt")') # prod_brand

        if product_brand:
            product[self.fields[4]] = product_brand.extract_first().encode('utf-8')
        else:
            product[self.fields[4]] = "N/A"

        product[self.fields[5]] = response.css('#our_price_display::text').extract_first().replace('DT','').replace(',','.').replace('.000','').replace(' ','').strip() # prod_price
        product[self.fields[6]] = response.request.url # prod_link

        self.product_crawled_counter += 1
        print('++++ ' + str(self.product_crawled_counter) + ' crawled products ++++')
        self.parsed_products.append(product)

    def write_to_csv(self):
        self.write_csv_headers()
        sorted_products = sorted(self.parsed_products, key=lambda k: k[self.fields[1]])
        for product in sorted_products:
            self.write_csv_entry(product)

    def write_csv_headers(self):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(field) 
                                    for field in self.fields)))
        
    def write_csv_entry(self, entry):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(entry[field]) 
                                for field in self.fields)))