import scrapy
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException
import pickle

urls = [
    'https://www.proxi.tn/303-types-de-consommables',
    'https://www.proxi.tn/351-classements-archivages'

]
filename = "proxi_urls"
products_urls = list()
keep_crawling = True

def main():
    # Specifying incognito mode as you launch your browser[OPTIONAL]
    option = webdriver.ChromeOptions()
    option.add_argument("--no-sandbox")
    option.add_argument("--start-maximized")
    browser = webdriver.Chrome(chrome_options=option)
    start_requests(browser)

def start_requests(browser):
    global keep_crawling
    for url in urls:
        keep_crawling = True
        print("Crawling url under >> " + url)
        crawl_request(browser, url, True)
    ulrs_to_file()

def crawl_request(browser, url, first_crawl):
    print('crawl_request')
    global keep_crawling
    if first_crawl:
        browser.get(url)
    try:
        if first_crawl:
            WebDriverWait(browser, 5).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="megamenu_wrapper"]/a')))
        else:
            browser.execute_script("window.scrollTo(0, 0);")
            WebDriverWait(browser, 5).until(EC.invisibility_of_element_located((By.XPATH, '//*[@id="product_list"]/p')))
        
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight / 2);")
        save_urls(browser)
        while keep_crawling:
            try:
                next_el = browser.find_element_by_css_selector('#pagination_next:not(.disabled)')
                next_el.click() 
                print('Still crawling')
                crawl_request(browser, url, False)
            except NoSuchElementException:
                print "Done!"
                keep_crawling = False
    except TimeoutException:
        print("Timed out waiting for page to load")
        browser.quit()


def save_urls(browser):
    products_anchors = browser.find_elements_by_css_selector("h3.grille a")
    for anchor in products_anchors:
        products_urls.append(anchor.get_attribute('href'))


def ulrs_to_file():
    global filename
    with open(filename, 'wb') as f:
        pickle.dump(products_urls, f)


main()