import scrapy
import csv
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher

class ProductSpider(scrapy.Spider):

    seller = "Mytek"
    name = "products_mytek"
    base = "http://www.mytek.tn"
    filename = "products_mytek.csv"
    fields = ["product_seller", "product_category", "product_name", "product_ref", "product_brand", "product_price", "product_link"]
    product_crawled_counter = 0
    parsed_products = list()

    def __init__(self):
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        self.write_to_csv()

    def start_requests(self):
        urls = [
            'http://www.mytek.tn/12-peripheriques-accessoires',
            'http://www.mytek.tn/290-photocopieurs',
            'http://www.mytek.tn/27-imprimantes-tunisie',
            'http://www.mytek.tn/123-consommables-tunisie',
            'http://www.mytek.tn/212-accessoires-imprimantes',
            'http://www.mytek.tn/143-projection',
            'http://www.mytek.tn/146-accessoires-projection-tunisie',
            'http://www.mytek.tn/43-son-numerique-tunisie',
            'http://www.mytek.tn/100-cables-tunisie'            
        ]
        for url in urls:
            print("Crawling url under >> " + url)
            yield scrapy.Request(url=url, callback=self.parse_category)

    def parse_category(self, response): # Crawling listed products
        for href in response.css("a.product-name::attr(href)").extract():
            print("Crawling product under >> " + href)
            yield scrapy.Request(url=href, callback=self.parse_product)
        next = response.xpath('//*[@id="pagination_next"]/a/@href').extract_first()
        if next: # Crawling next category pages
            href = self.base + next
            print("Crawling category page under >> " + href)
            yield scrapy.Request(url=href, callback=self.parse_category)

    def parse_product(self, response): # Crawling product
        product = dict() 
        product[self.fields[0]] = self.seller # prod_seller
        product_category = response.xpath('//*[@id="columns"]/div[1]/div/span[2]/span[3]/a/span/text()') # prod_category
        if product_category:
            product[self.fields[1]] = product_category.extract_first().encode('utf-8')
        else:
            product[self.fields[1]] = "N/A"

        product[self.fields[2]] = response.css('h1::text').extract_first().replace(',',' ').encode('utf-8') # prod_name
        product[self.fields[3]] = response.css('#product_reference span::text').extract_first() # prod_ref
        product_brand = response.css('div.manu a::text') # prod_brand

        if product_brand:
            product[self.fields[4]] = product_brand.extract_first().encode('utf-8')
        else:
            product[self.fields[4]] = "N/A"

        product[self.fields[5]] = response.css('#our_price_display::text').extract_first().replace('DT','').replace(',','.').replace('.000','').replace(' ','').strip() # prod_price
        product[self.fields[6]] = response.request.url # prod_link
        
        self.product_crawled_counter += 1
        print('++++ ' + str(self.product_crawled_counter) + ' crawled products ++++')
        self.parsed_products.append(product)

    def write_to_csv(self):
        self.write_csv_headers()
        sorted_products = sorted(self.parsed_products, key=lambda k: k[self.fields[1]])
        for product in sorted_products:
            self.write_csv_entry(product)

    def write_csv_headers(self):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(field) 
                                    for field in self.fields)))
        return
        
    def write_csv_entry(self, entry):
        with open(self.filename,'a+') as f:
            f.write("{}\n".format('\t'.join(str(entry[field]) 
                                for field in self.fields)))